#### BUILD IMAGE:

##### 1) Build from Dockerfile:

` docker build -t vtiger . ` (in the same path of your Dockerfile)


#### RUN CONTAINER:

Use docker compose or the following commands below

You need a DB container, you can choose between:

##### 1) MariaDB:
` docker run --name mariadb -d -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=vtiger -e MYSQL_USER=vtiger_user -e MYSQL_PASSWORD=pwd --net=host mariadb `

##### 2) MySQL:
` docker run --name mysql -d -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=vtiger -e MYSQL_USER=vtiger_user -e MYSQL_PASSWORD=pwd --net=host mysql:5.5 `

Then run Vtiger container:

` docker run --name vtiger -d -e DB_HOSTNAME=127.0.0.1 -e DB_USERNAME=vtiger_user -e DB_PASSWORD=pwd -e DB_NAME=vtiger -p 80:80 --net=host pimuzzo/vtiger `

#### CONFIGURATION:
localhost is the docker container ip.
Follow the steps: [http://localhost:8888/vtigercrm](http://localhost:8888/vtigercrm)
When prompted select create database and use the MYSQL user and password from the docker-compose file
